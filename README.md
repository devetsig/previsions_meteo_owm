# Application de prévisions météo

> Petite application de prévisions météorologiques développée avec le framework VueJS et l'API OpenWeatherMap. Pour accéder à l'application : [https://previsions-meteo-owm-699be.web.app/](https://previsions-meteo-owm-699be.web.app/)

![](src/assets/app.png)

## Fonctionnement

A l'aide du générateur de projet Vue-CLI, est créé un projet VueJS avec un template Webpack. Les données de l'API OpenWeatherMap sont appelées grâce au client HTTP Axios puis stockées dans le store VueX (bibliothèque de gestion d'états) pour enfin être redistribuées dans des composants Vuetify. Vue2-Leaflet accélère la création de l'interface cartographique tandis que Vue-Chartkick génère le graphique linéaire.