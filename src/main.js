import Vue from 'vue'
import App from './App'
import vuetify from '@/plugins/vuetify'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import L from 'leaflet'
import 'leaflet/dist/leaflet.css'
import Chartkick from 'vue-chartkick'
import Chart from 'chart.js'
import {store} from './store/store'
import ChartAnnotationsPlugin from 'chartjs-plugin-annotation'

Chart.plugins.register(ChartAnnotationsPlugin)


Vue.use(Chartkick.use(Chart))
Vue.use(vuetify, {
  iconfont: 'md'
})

Vue.config.productionTip = false

delete L.Icon.Default.prototype._getIconUrl  
L.Icon.Default.mergeOptions({  
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),  
  iconUrl: require('leaflet/dist/images/marker-icon.png'),  
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')  
})

new Vue({
  vuetify,
  store: store,
  el: '#app',
  components: { App },
  template: '<App/>'
})

