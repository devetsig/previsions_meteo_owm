import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
import { pushd } from 'shelljs'

Vue.use(Vuex, Axios)

export const store = new Vuex.Store({
    strict: true,
    state: {
        chart: null,
        ville: "",
        loading: false,
        map : null,
        url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        attribution:'&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
        zoom: 10,
        center: [49.03898, 2.07501],
        show_wmap: true,
        marker: null,
        posts: [],
        dataApi: {
            temperatures: [],
            dates: [],
            humidite: [],
            vent: []
        },
        temps: [],
        dataApiBis: null,
        windTemps: null,
        humTemps: null,
    },
    actions: {
        getTemps: function() {

          this.state.loading = true;

          Axios
                .get("http://api.openweathermap.org/data/2.5/forecast", {
                    params: {
                      q: this.state.ville,
                      units: "metric",
                      appid: ""//entrez votre identifiant API
                    }
                  })
                .then(response => {
                    this.state.temps = response.data;
                    this.state.center = response.data.city.coord;
                    this.state.dataApi.dates = response.data.list.map(list => {
                        return list.dt_txt;
                      });
                    this.state.dataApi.temperatures = response.data.list.map(list => {
                        return list.main.temp;
                      });
                    this.state.dataApi.humidite = response.data.list.map(list => {
                        return list.main.humidity;
                      });
                    this.state.dataApi.vent = response.data.list.map(list => {
                        return list.wind.speed;
                      });
                                        
                    var truc = {};
                    for(var i=0; i<this.state.dataApi.dates.length; i++) {
                        truc[this.state.dataApi.dates[i]] = this.state.dataApi.temperatures[i];
                    }
                    this.state.dataApiBis = truc

                    var x = {};
                    for(var i=0; i<this.state.dataApi.dates.length; i++) {
                        x[this.state.dataApi.dates[i]] = this.state.dataApi.humidite[i];
                    }
                    this.state.humTemps = x

                    var y = {};
                    for(var i=0; i<this.state.dataApi.dates.length; i++) {
                        y[this.state.dataApi.dates[i]] = this.state.dataApi.vent[i];
                    }
                    this.state.windTemps = y
                    
                })
                .catch(error => {
                    console.log(error);
                  })
        }
    }
})